import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'br.com.mobicare.oiads.sample',
  appName: 'Oi Ads Sample',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
